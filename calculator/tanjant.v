`timescale 1ns / 1ps

module tanjant(

input [31:0]sayi1,
input clk,
output reg [63:0]sonuc,
output reg tasma,
output reg gecerli
    );
    reg  [63:0] sonuc_next;
    reg tasma_next;
    reg [127:0]tan=0;
   
    reg [31:0] temp=0;
    reg [31:0] temp1=0;
    
    reg [255:0]kare=0;
    reg [255:0]us_temp[5:0];
    
    reg [128:0] katsayi [5:0];
    
    reg [127:0] us [5:0];
    reg [127:0] terim [5:0];
    reg [255:0]terim_temp[5:0];
    reg pi=32'h0003243f;
    reg pi_2=32'h0001921f;
    
    
    integer i;
    
    initial begin
        sonuc_next=0;
        tasma_next=0;
        us[0]=0;
        //gecerli=0;
    
        
        katsayi[0]=128'h00000000000000010000000000000000;
        katsayi[1]=128'h00000000000000005555000000000000;
        katsayi[2]=128'h00000000000000002222000000000000;
        katsayi[3]=128'h00000000000000000DD0000000000000;
        katsayi[4]=128'h00000000000000000599000000000000;
        katsayi[5]=128'h00000000000000000244000000000000;
    
    end


    always @(*) begin
        
        temp=sayi1%32'h0003243f;
        
        if (temp<32'h0001921f) begin
            us[0][79:48]=temp;
             
        end
        else if (temp>32'h0001921f) begin
            temp1=temp-32'h0001921f;
            us[0][79:48]=temp1;
            end
                
        kare=us[0]*us[0];
        
        for(i=0;i<5;i=i+1)begin
            us_temp[i]=us[i]*kare[191:64]; //x3
            us[i+1]=us_temp[i][191:64];
        end
                   
        //terimleri hesapla
       for(i=0;i<6;i=i+1)begin
            terim_temp[i]=us[i]*katsayi[i]; //x3
            terim[i]=terim_temp[i][191:64]; 
        end  
        
         //topla
        tan=terim[0]+terim[1]+terim[2]+terim[3]+terim[4]+terim[5];   
        if (temp<32'h0001921f) begin
            
            sonuc_next=tan[95:32];
           
        end        
        else if (temp>32'h0001921f) begin
            
            sonuc_next=tan[95:32];
            sonuc_next[63]=1;
            
        end
        
        
        else if (temp==32'h0001921f)begin
            sonuc_next=(1/0);
        end
       
        // tasma
        if (sayi1>32'h00019211d) begin
        tasma_next=1;
        end
    end
always @ (posedge clk)begin
sonuc<=sonuc_next;
tasma<=0;
gecerli<=1;
end
endmodule
