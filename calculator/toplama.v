`timescale 1ns / 1ps

module toplama(
input clk,
input [31:0] sayi1 ,
input [31:0] sayi2 ,
output reg [63:0] sonuc ,
output reg tasma,
output reg gecerli
);

integer i,j;
reg carry1;
reg carry2;
reg  [31:0] sonuc1;
reg  [63:0] sonuc_next;

initial begin
carry1=0;
carry2=0;
sonuc1=0;
sonuc_next=0;
end


always@(posedge clk) begin
sonuc<=sonuc_next;
tasma<=0;
gecerli=1;
end


always@(*) begin
for( i=0; i<16; i=i+1) begin
if((sayi1[i])==0 && (sayi2[i]==0) && (carry1==0)) begin
sonuc1[i]=0;
carry1=0;
end

else if ((sayi1[i]==0) && (sayi2[i]==0) && (carry1==1)) begin
sonuc1[i]=1;
carry1=0;
end

else if (sayi1[i]==0 && sayi2[i]==1 && carry1==0)  begin
sonuc1[i]=1;
carry1=0;
end

else if (sayi1[i]==1 && sayi2[i]==0 && carry1==0) begin
sonuc1[i]=1;
carry1=0;
end

else if(sayi1[i]==0 && sayi2[i]==1 && carry1==1)  begin
sonuc1[i]=0;
carry1=1;
end

else if(sayi1[i]==1 && sayi2[i]==0 && carry1==1)  begin
sonuc1[i]=0;
carry1=1;
end

else if(sayi1[i]==1 && sayi2[i]==1 && carry1==0) begin
sonuc1[i]=0;
carry1=1;
end

else if((sayi1[i]==1) && (sayi2[i]==1) && (carry1==1)) begin
sonuc1[i]=1;
carry1=1;
end
end

sonuc_next={sonuc1,16'b0000000000000000};
carry2=carry1;

for( j=16; j<32; j=j+1) begin
if((sayi1[j])==0 && (sayi2[j]==0) && (carry2==0)) begin
sonuc_next[j+16]=0;
carry2=0;
end

else if ((sayi1[j]==0) && (sayi2[j]==0) && (carry2==1)) begin
sonuc_next[j+16]=1;
carry2=0;
end

else if (sayi1[j]==0 && sayi2[j]==1 && carry2==0)  begin
sonuc_next[j+16]=1;
carry2=0;
end

else if (sayi1[j]==1 && sayi2[j]==0 && carry2==0) begin
sonuc_next[j+16]=1;
carry2=0;
end

else if(sayi1[j]==0 && sayi2[j]==1 && carry2==1)  begin
sonuc_next[j+16]=0;
carry2=1;
end

else if(sayi1[j]==1 && sayi2[j]==0 && carry2==1)  begin
sonuc_next[j+16]=0;
carry2=1;
end

else if(sayi1[j]==1 && sayi2[j]==1 && carry2==0) begin
sonuc_next[j+16]=0;
carry2=1;
end

else if((sayi1[j]==1) && (sayi2[j]==1) && (carry2==1)) begin
sonuc_next[j+16]=1;
carry2=1;
end
end

//bir sonraki bite tasma kontrolu
if(sayi1[31]==0 && sayi2[31]==1 && carry2==1) begin
sonuc_next[48]=1;
end

else if(sayi1[31]==1 && sayi2[31]==0 && carry2==1) begin
sonuc_next[48]=1;
end

else if(sayi1[31]==1 && sayi2[31]==1 && carry2==0) begin
sonuc_next[48]=1;
end

else if(sayi1[31]==1 && sayi2[31]==1 && carry2==1) begin
sonuc_next[48]=1;
end

end
endmodule

