`timescale 1ns / 1ps

module main(
    input [31:0]sayi1,sayi2,
    input [2:0]tur,
    input clk,
    input rst,
    output reg gecerli,
    output reg tasma,
    output reg hazir,
    output reg [63:0]sonuc
);

reg  [63:0] sonuc_next;
reg tasma_next;
reg gecerli_next;

initial begin
sonuc_next=0;
tasma_next=0;
gecerli_next=0;
end

always@(posedge clk) begin
if(rst!=1) begin
sonuc<=sonuc_next;
tasma<=tasma_next;
gecerli<=gecerli_next;
end
else begin
sonuc<=0;
tasma<=0;
gecerli<=0;
end
end 

//000-toplama,001-cikarma,010-carpma,011-bolme,100-karekok,101-tanjant,110-kotanjant

wire [63:0] sonuc0; wire gecerli0; wire tasma0;
toplama top(
.clk (clk),
.sayi1 (sayi1),
.sayi2 (sayi2),
.sonuc (sonuc0),
.tasma (tasma0),
.gecerli (gecerli0)
);
        
wire [63:0] sonuc1; wire gecerli1; wire tasma1;      
cikarma cik(
.clk (clk),
.sayi1 (sayi1),
.sayi2 (sayi2),
.sonuc (sonuc1),
.tasma (tasma1),
.gecerli (gecerli1)
);
        
wire [63:0] sonuc2; wire gecerli2; wire tasma2;      
carpma carp(
.clk (clk),
.sayi1 (sayi1),
.sayi2 (sayi2),
.sonuc (sonuc2),
.tasma (tasma2),
.gecerli (gecerli2)
);
        
wire [63:0] sonuc3; wire gecerli3; wire tasma3;
bolme bol(
.clk (clk),
.sayi1 (sayi1),
.sayi2 (sayi2),
.sonuc (sonuc3),
.tasma (tasma3),
.gecerli (gecerli3)
);
        
wire [63:0] sonuc4; wire gecerli4; wire tasma4;
karekok kare(
.clk (clk),
.sayi (sayi1),
.sonuc (sonuc4),
.tasma (tasma4),
.gecerli (gecerli4)
); 
        
wire [63:0] sonuc5; wire gecerli5; wire tasma5;
tanjant tan(
.clk (clk),
.sayi1 (sayi1),
.sonuc (sonuc5),
.tasma (tasma5),
.gecerli (gecerli5)
); 
        
wire [63:0] sonuc6; wire gecerli6; wire tasma6;
kotanjant cot(
.clk (clk),
.sayi1 (sayi1),
.sonuc (sonuc6),
.tasma (tasma6),
.gecerli (gecerli6)
); 

    
always@(*) begin  
if(tur == 3'b000) begin
sonuc_next=sonuc0;
tasma_next=tasma0;
gecerli_next=gecerli0;
hazir=1;   
    end 
    else if (tur == 3'b001) begin
          sonuc_next=sonuc1;
          tasma_next=tasma1;
          gecerli_next=gecerli1;
          hazir=1;  
    end
    else if(tur == 3'b010) begin
          sonuc_next=sonuc2;
          tasma_next=tasma2;
          gecerli_next=gecerli2;
          hazir=1;  
    end
    else if(tur == 3'b011) begin
          sonuc_next=sonuc3;
          tasma_next=tasma3;
          gecerli_next=gecerli3;
          hazir=1;      
    end
    else if(tur == 3'b100) begin
          sonuc_next=sonuc4;
          tasma_next=tasma4;
          gecerli_next=gecerli4;
          hazir=1;      
    end  
    else if(tur == 3'b101) begin
          sonuc_next=sonuc5;
          tasma_next=tasma5;
          gecerli_next=gecerli5;
          hazir=1;
    end
    else if(tur == 3'b110) begin
          sonuc_next=sonuc6;
          tasma_next=tasma6;
          gecerli_next=gecerli6;
          hazir=1;
    end
end
  
endmodule