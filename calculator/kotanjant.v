`timescale 1ns / 1ps

module kotanjant(
    input [31:0]sayi1,
    input clk,
    //input rst,
    output reg [63:0]sonuc,
    output reg gecerli,
    output reg tasma
  
    );
    
    wire tasmatan;
    wire [63:0] sonuctan;
    reg [63:0]sonuc_next;
    wire gecerlitan;
    reg tasma_next;
    
    reg [31:0]temp=0;
    reg [31:0]temptan;
    reg pi_2=32'h0001921f;
    reg [1:0] sayac=0;
    reg [1:0] sayac_next=0;
    
    initial begin 
        tasma_next=0;
        sonuc_next=0;
    end
    
    tanjant tangent1(.sayi1(temptan), .clk(clk),.tasma(tasmatan), .sonuc(sonuctan), .gecerli(gecerlitan));
    
    always @(*) begin
        temp=32'h0001921f+sayi1;
        temptan=temp;
        sonuc_next=sonuctan;
        tasma_next=tasmatan;
        sayac_next=sayac;
    end
    
    always@(posedge clk) begin
        sonuc<=sonuc_next;
        tasma<=tasma_next;
        if(sayac_next==2'b01)begin
        sayac<=2'b00;
        gecerli<=1;
        end
        else begin
        sayac<=sayac_next+1;
        end
    end
    
endmodule