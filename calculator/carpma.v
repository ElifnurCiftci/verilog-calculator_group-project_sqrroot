`timescale 1ns / 1ps

module carpma(
input [31:0]sayi1,sayi2,
input clk,
output reg gecerli,
output reg tasma,
output reg[63:0]sonuc
);
integer i;
reg initialbit=0;
reg  [63:0] sonuc_next;
reg tasma_next;

initial begin
sonuc_next[63:32]=32'b0;
sonuc_next[31:0]=sayi2;
end

always @ (posedge clk)begin
sonuc<=sonuc_next;
tasma<=0;
gecerli<=1;
end 

always@ (*) begin
    for(i=0; i<32; i=i+1) begin
        if ((initialbit==0)&&(sayi2[i]==1)) begin
        initialbit=sayi2[i];
        sonuc_next[63:32]=sonuc_next[63:32]-sayi1;
        sonuc_next=sonuc_next>>1;
        sonuc_next[63]=sayi2[i];
        end
        else if((initialbit==1)&&(sayi2[i]==0)) begin
        initialbit=sayi2[i];
        sonuc_next[63:32]=sonuc_next[63:32]+sayi1;
        sonuc_next=sonuc_next>>1;
        sonuc_next[63]=sayi2[i];
        end
        else if(((initialbit==1)&&(sayi2[i]==1))|| ((initialbit==0)&&(sayi2[i]==0))) begin
        initialbit=sayi2[i];
        sonuc_next=sonuc_next>>1;
        sonuc_next[63]=sayi2[i];
        end   
    end
end
endmodule
