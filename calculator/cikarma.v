`timescale 1ns / 1ps

module cikarma(
input clk,
input [31:0] sayi1 ,
input [31:0] sayi2 ,
output reg [63:0] sonuc ,
output reg tasma,
output reg gecerli
);

reg [31:0] sayi12;
wire [63:0] sonuc1;
wire [63:0] sonuc2;
reg [63:0] sonuc_next;
wire tasma1;
wire tasma2;
wire gecerli1;
wire gecerli2;
reg [1:0] sayac=0;
reg [1:0] sayac_next=0;

always@(posedge clk) begin
sonuc<=sonuc_next;
tasma<=0;
if(sayac_next==2'b11)begin
sayac<=2'b01;
gecerli<=1;
end
else begin
sayac<=sayac_next+1;
end
end 

initial begin
sayi12=32'b00000000000000000000000000000001;
gecerli=0;
end

toplama uut1(
.clk(clk),
.sayi1(~sayi2),
.sayi2(sayi12),
.sonuc(sonuc1),
.tasma(tasma1),
.gecerli(gecerli1)
);

wire [31:0] sonuc13;
assign sonuc13=sonuc1[47:16];

toplama uut2(
.clk(clk),
.sayi1(sayi1),
.sayi2(sonuc13),
.sonuc(sonuc2),
.tasma(tasma2),
.gecerli(gecerli2)
);

always@* begin
sonuc_next=sonuc2;
sonuc_next[48]=1'b0;
sayac_next=sayac;
end
endmodule
