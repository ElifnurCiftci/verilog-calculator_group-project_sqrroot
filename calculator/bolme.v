`timescale 1ns / 1ps

    module bolme(
        input [31:0] sayi1, 
        input [31:0] sayi2,
        input clk,
        output reg gecerli,
        output reg tasma,
        output reg [63:0] sonuc
        ); 
		
    reg [63:0] sonuc_next;		
		
    always@(posedge clk) begin
        sonuc<=sonuc_next;
        tasma<=0;
        if(sayi2!=0) begin
            gecerli=1;
        end
        else begin
            gecerli=0;
        end
    end       
		
    reg [63:0] tmp=0;
    
    always@(*) begin
      if(sayi2==0) begin
          sonuc_next=0;
      end
      else begin
          tmp[63:32]=sayi1;  
          tmp=tmp/sayi2;
          sonuc_next=tmp;
      end
    end

	endmodule
