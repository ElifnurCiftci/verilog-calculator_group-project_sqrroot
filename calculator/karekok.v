`timescale 1ns / 1ps

module karekok(
        input [31:0] sayi, 
        input clk,
        output reg gecerli,
        output reg tasma,
        output reg [63:0] sonuc
    );
    
   integer i=0;
   wire[15:0] n=sayi [31:16];
   reg [15:0] r=0;
   
   reg [63:0] sonuc_next=0;		
		
   always@(posedge clk) begin
        sonuc<=sonuc_next;
        tasma<=0;
        gecerli=1;
    end       
   
   
   always@(*) begin
       if(sayi==0)begin
	sonuc_next=64'd0;
       end
       else if(sayi>0)begin
           r=n;
           for(i=0;i<10;i=i+1) begin
               r=((r+(n/r))/2);
           end
           sonuc_next[47:32]=r;
       end
   end 
    
endmodule
